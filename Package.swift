// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "CombinedSox",
    targets: [
        .target(
            name: "CombinedSox",
            dependencies: ["soxu"],
            path: "Sources/CombinedSox"
        ),
        .systemLibrary(
            name: "soxu", 
            path: "Sources/soxu",
            providers: [
                SystemPackageProvider.brew(["sox"]),
                SystemPackageProvider.apt(["sox"])
            ]
        ),
        .testTarget(
            name: "CombinedSoxTests",
            dependencies: ["CombinedSox"]
        ),
    ]
)
