# CombinedSox

## Description

This is the project to embed play with C library embeding into Swift package namely.
The library is [sox](http://sox.sourceforge.net).

## Usage

1. `brew install sox`
2. `swift package update`
3. `swift run`

There's should be follow output: 
```
This is the sox library var SOX_SUCCESS.rawValue: 0
```
The 0 — is the value of the sox library variable.
So the library is linked and available within the code.
