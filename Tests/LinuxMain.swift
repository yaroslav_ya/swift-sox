import XCTest

import CombinedSoxTests

var tests = [XCTestCaseEntry]()
tests += CombinedSoxTests.allTests()
XCTMain(tests)
